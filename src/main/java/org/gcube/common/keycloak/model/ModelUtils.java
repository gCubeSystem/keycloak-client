package org.gcube.common.keycloak.model;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.core.type.TypeReference;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.ObjectWriter;
import org.gcube.io.jsonwebtoken.ExpiredJwtException;
import org.gcube.io.jsonwebtoken.JwtException;
import org.gcube.io.jsonwebtoken.Jwts;
import org.gcube.io.jsonwebtoken.io.DeserializationException;
import org.gcube.io.jsonwebtoken.io.Deserializer;
import org.gcube.io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:mauro.mugnaini@nubisware.com">Mauro Mugnaini</a>
 */
public class ModelUtils {

    protected static final Logger logger = LoggerFactory.getLogger(ModelUtils.class);

    private static final String ACCOUNT_AUDIENCE_RESOURCE = "account";

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setSerializationInclusion(Include.NON_NULL);
    }

    public static String toJSONString(Object object) {
        return toJSONString(object, false);
    }

    public static String toJSONString(Object object, boolean prettyPrint) {
        ObjectWriter writer = prettyPrint ? mapper.writerWithDefaultPrettyPrinter() : mapper.writer();
        try {
            return writer.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Cannot pretty print object", e);
            return null;
        }
    }

    /**
     * Creates a {@link RSAPublicKey} instance from its string PEM representation
     * 
     * @param publicKeyPem the public key PEM string
     * @return the RSA public key
     * @throws Exception if it's not possible to create the RSA public key from the PEM string
     */
    public static RSAPublicKey createRSAPublicKey(String publicKeyPem) throws Exception {
        return (RSAPublicKey) createPublicKey(publicKeyPem, "RSA");
    }

    /**
     * Creates a {@link PublicKey} instance from its string PEM representation
     * 
     * @param publicKeyPem the public key PEM string
     * @param algorithm the key type (e.g. RSA)
     * @return the public key
     * @throws Exception if it's not possible to create the public key from the PEM string
     */
    public static PublicKey createPublicKey(String publicKeyPem, String algorithm) throws Exception {
        try {
            String publicKey = publicKeyPem.replaceFirst("-----BEGIN (.*)-----\n", "");
            publicKey = publicKey.replaceFirst("-----END (.*)-----", "");
            publicKey = publicKey.replaceAll("\r\n", "");
            publicKey = publicKey.replaceAll("\n", "");

            byte[] encoded = Base64.getDecoder().decode(publicKey);
            KeyFactory kf = KeyFactory.getInstance(algorithm);
            return kf.generatePublic(new X509EncodedKeySpec(encoded));
        } catch (Exception e) {
            throw new RuntimeException("Cannot create public key from PEM string", e);
        }
    }

    /**
     * Verifies the token validity
     * 
     * @param token the base64 JWT token string
     * @param publicKey the realm's public key on server
     * @return <code>true</code> if the token is valid, <code>false</code> otherwise
     * @throws Exception if an error occurs constructing the verifier
     */
    public static boolean isValid(String token, PublicKey publicKey) throws Exception {
        return isValid(token, publicKey, true);
    }

    /**
     * Verifies the token validity
     * 
     * @param token the base64 JWT token string
     * @param publicKey the public key to use for verification
     * @param checkExpiration if <code>false</code> token expiration check is disabled
     * @return <code>true</code> if the token is valid, <code>false</code> otherwise
     * @throws Exception if an unexpected error occurs (e.g. constructing the verifier)
     */
    public static boolean isValid(String token, PublicKey publicKey, boolean checkExpiration) throws Exception {
        try {
            verify(token, publicKey);
            return true;
        } catch (ExpiredJwtException e) {
            // This is OK because expiration check is after the signature validation in the implementation
            if (logger.isDebugEnabled()) {
                logger.debug("JWT is expired: {}", e.getMessage());
            }
            return !checkExpiration;
        } catch (SignatureException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("JWT signature is not verified: {}", e.getMessage());
            }
            return false;
        } catch (JwtException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("JWT object is not valid: {}", e.getMessage());
            }
            return false;
        }
    }

    /**
     * Verifies the token signature and expiration
     * 
     * @param token the base64 JWT token string
     * @param publicKey the public key to use for verification
     * @throws SignatureException if the token signature is invalid
     * @throws ExpiredJwtException if the token is expired
     * @throws JwtException if a JWT related problem is found
     * @throws Exception if an unexpected error occurs (e.g. constructing the verifier)
     */
    public static void verify(String token, PublicKey publicKey)
            throws SignatureException, ExpiredJwtException, JwtException, Exception {

        Jwts.parser().json(new GcubeJacksonDeserializer()).verifyWith(publicKey).build().parse(token);
    }

    public static String getAccessTokenPayloadJSONStringFrom(TokenResponse tokenResponse) throws Exception {
        return getAccessTokenPayloadJSONStringFrom(tokenResponse, true);
    }

    public static String getAccessTokenPayloadJSONStringFrom(TokenResponse tokenResponse, boolean prettyPrint)
            throws Exception {

        return toJSONString(getAccessTokenFrom(tokenResponse, Object.class), prettyPrint);
    }

    public static AccessToken getAccessTokenFrom(TokenResponse tokenResponse) throws Exception {
        return getAccessTokenFrom(tokenResponse, AccessToken.class);
    }

    public static AccessToken getAccessTokenFrom(String authorizationHeaderOrBase64EncodedJWT) throws Exception {
        return getAccessTokenFrom(authorizationHeaderOrBase64EncodedJWT.matches("[b|B]earer ")
                ? authorizationHeaderOrBase64EncodedJWT.substring("bearer ".length())
                : authorizationHeaderOrBase64EncodedJWT, AccessToken.class);
    }

    private static <T> T getAccessTokenFrom(TokenResponse tokenResponse, Class<T> clazz) throws Exception {
        return getAccessTokenFrom(tokenResponse.getAccessToken(), clazz);
    }

    private static <T> T getAccessTokenFrom(String accessToken, Class<T> clazz) throws Exception {
        return mapper.readValue(getDecodedPayload(accessToken), clazz);
    }

    public static String getRefreshTokenPayloadStringFrom(TokenResponse tokenResponse) throws Exception {
        return getRefreshTokenPayloadStringFrom(tokenResponse, true);
    }

    public static String getRefreshTokenPayloadStringFrom(TokenResponse tokenResponse, boolean prettyPrint)
            throws Exception {

        return toJSONString(getRefreshTokenFrom(tokenResponse, Object.class), prettyPrint);
    }

    public static RefreshToken getRefreshTokenFrom(TokenResponse tokenResponse) throws Exception {
        return getRefreshTokenFrom(tokenResponse.getRefreshToken());
    }

    public static RefreshToken getRefreshTokenFrom(String base64EncodedJWT) throws Exception {
        return mapper.readValue(getDecodedPayload(base64EncodedJWT), RefreshToken.class);
    }

    private static <T> T getRefreshTokenFrom(TokenResponse tokenResponse, Class<T> clazz) throws Exception {
        return mapper.readValue(getDecodedPayload(tokenResponse.getRefreshToken()), clazz);
    }

    protected static byte[] getBase64Decoded(String string) {
        return Base64.getDecoder().decode(string);
    }

    protected static String splitAndGet(String encodedJWT, int index) {
        String[] split = encodedJWT.split("\\.");
        if (split.length == 3) {
            return split[index];
        } else {
            return null;
        }
    }

    public static byte[] getDecodedHeader(String value) {
        return getBase64Decoded(getEncodedHeader(value));
    }

    public static String getEncodedHeader(String encodedJWT) {
        return splitAndGet(encodedJWT, 0);
    }

    public static byte[] getDecodedPayload(String value) {
        return getBase64Decoded(getEncodedPayload(value));
    }

    public static String getEncodedPayload(String encodedJWT) {
        return splitAndGet(encodedJWT, 1);
    }

    public static byte[] getDecodedSignature(String value) {
        return getBase64Decoded(getEncodedSignature(value));
    }

    public static String getEncodedSignature(String encodedJWT) {
        return splitAndGet(encodedJWT, 2);
    }

    public static String getClientIdFromToken(AccessToken accessToken) {
        String clientId;
        logger.debug("Client id not provided, using authorized party field (azp)");
        clientId = accessToken.getIssuedFor();
        if (clientId == null) {
            logger.warn("Issued for field (azp) not present, getting first of the audience field (aud)");
            clientId = getFirstAudienceNoAccount(accessToken);
        }
        return clientId;
    }

    private static String getFirstAudienceNoAccount(AccessToken accessToken) {
        // Trying to get it from the token's audience ('aud' field), getting the first except the 'account'
        List<String> tokenAud = Arrays.asList(accessToken.getAudience());
        tokenAud.remove(ACCOUNT_AUDIENCE_RESOURCE);
        if (tokenAud.size() > 0) {
            return tokenAud.iterator().next();
        } else {
            // Setting it to empty string to avoid NPE in encoding
            return "";
        }
    }

    public static class GcubeJacksonDeserializer implements Deserializer<Map<String, ?>> {

        @Override
        public Map<String, ?> deserialize(byte[] bytes) throws DeserializationException {
            return deserialize(new InputStreamReader(new ByteArrayInputStream(bytes)));
        }

        @Override
        public Map<String, ?> deserialize(Reader reader) throws DeserializationException {
            try {
                return new ObjectMapper().readValue(reader, new TypeReference<HashMap<String, Object>>() {
                });
            } catch (IOException e) {
                throw new DeserializationException("Cannot deserialize JSON with GCube Jackson", e);
            }
        }

    }
}