/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gcube.common.keycloak.model;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 */
public class RefreshToken extends AccessToken {

    private static final long serialVersionUID = 2646534143077862960L;

    public RefreshToken() {
    }

    public RefreshToken(AccessToken token) {
        super();
        this.issuer = token.issuer;
        this.subject = token.subject;
        this.issuedFor = token.issuedFor;
        this.sessionState = token.sessionState;
        this.nonce = token.nonce;
        this.audience = new String[] { token.issuer };
        this.scope = token.scope;
    }

}
