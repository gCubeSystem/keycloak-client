/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gcube.common.keycloak.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 */
public class AccessToken extends IDToken {

    private static final long serialVersionUID = 6364784008775737335L;

    public static class Access implements Serializable {

        private static final long serialVersionUID = 1634782115467850693L;

        @JsonProperty("roles")
        protected Set<String> roles;

        @JsonProperty("verify_caller")
        protected Boolean verifyCaller;

        public Access() {
        }

        public Access clone() {
            Access access = new Access();
            access.verifyCaller = verifyCaller;
            if (roles != null) {
                access.roles = new HashSet<>();
                access.roles.addAll(roles);
            }
            return access;
        }

        public Set<String> getRoles() {
            return roles;
        }

        public Access roles(Set<String> roles) {
            this.roles = roles;
            return this;
        }

        @JsonIgnore
        public boolean isUserInRole(String role) {
            if (roles == null)
                return false;
            return roles.contains(role);
        }

        public Access addRole(String role) {
            if (roles == null)
                roles = new HashSet<>();
            roles.add(role);
            return this;
        }

        public Boolean getVerifyCaller() {
            return verifyCaller;
        }

        public Access verifyCaller(Boolean required) {
            this.verifyCaller = required;
            return this;
        }

        @Override
        public String toString() {
            return getRoles() != null ? getRoles().toString() : null;
        }
    }

    @JsonProperty("trusted-certs")
    protected Set<String> trustedCertificates;

    @JsonProperty("allowed-origins")
    protected Set<String> allowedOrigins;

    @JsonProperty("realm_access")
    protected Access realmAccess;

    @JsonProperty("resource_access")
    protected Map<String, Access> resourceAccess;

    @JsonProperty("scope")
    protected String scope;

    @JsonIgnore
    public Map<String, Access> getResourceAccess() {
        return resourceAccess == null ? Collections.<String, Access>emptyMap() : resourceAccess;
    }

    public void setResourceAccess(Map<String, Access> resourceAccess) {
        this.resourceAccess = resourceAccess;
    }

    public Access addAccess(String service) {
        if (resourceAccess == null) {
            resourceAccess = new HashMap<>();
        }

        Access access = resourceAccess.get(service);
        if (access != null)
            return access;
        access = new Access();
        resourceAccess.put(service, access);
        return access;
    }

    @Override
    public AccessToken id(String id) {
        return (AccessToken) super.id(id);
    }

    @Override
    public AccessToken issuer(String issuer) {
        return (AccessToken) super.issuer(issuer);
    }

    @Override
    public AccessToken subject(String subject) {
        return (AccessToken) super.subject(subject);
    }

    @Override
    public AccessToken type(String type) {
        return (AccessToken) super.type(type);
    }

    public Set<String> getAllowedOrigins() {
        return allowedOrigins;
    }

    public void setAllowedOrigins(Set<String> allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    public Access getRealmAccess() {
        return realmAccess;
    }

    public void setRealmAccess(Access realmAccess) {
        this.realmAccess = realmAccess;
    }

    public Set<String> getTrustedCertificates() {
        return trustedCertificates;
    }

    public void setTrustedCertificates(Set<String> trustedCertificates) {
        this.trustedCertificates = trustedCertificates;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

}
