package org.gcube.common.keycloak.model;

/**
 * @author <a href="mailto:mauro.mugnaini@nubisware.com">Mauro Mugnaini</a>
 */
public class OIDCConstants {

    public static final String PERMISSION_PARAMETER = "permission";
    public static final String GRANT_TYPE_PARAMETER = "grant_type";
    public static final String SCOPE_PARAMETER = "scope";
    public static final String CLIENT_CREDENTIALS_GRANT_TYPE = "client_credentials";
    public static final String UMA_TOKEN_GRANT_TYPE = "urn:ietf:params:oauth:grant-type:uma-ticket";
    public static final String TOKEN_EXCHANGE_GRANT_TYPE = "urn:ietf:params:oauth:grant-type:token-exchange";
    public static final String AUDIENCE_PARAMETER = "audience";
    public static final String REFRESH_TOKEN_GRANT_TYPE = "refresh_token";
    public static final String REFRESH_TOKEN_PARAMETER = "refresh_token";
    public static final String CLIENT_ID_PARAMETER = "client_id";
    public static final String CLIENT_SECRET_PARAMETER = "client_secret";
    public static final String TOKEN_PARAMETER = "token";
    public static final String PASSWORD_GRANT_TYPE = "password";
    public static final String USERNAME_PARAMETER = "username";
    public static final String PASSWORD_PARAMETER = "password";
    public static final String SUBJECT_TOKEN_PARAMETER = "subject_token";
    public static final String SUBJECT_TOKEN_TYPE_PARAMETER = "subject_token_type";
    public static final String REQUESTED_TOKEN_TYPE_PARAMETER = "requested_token_type";
    public static final String ACCESS_TOKEN_TOKEN_TYPE = "urn:ietf:params:oauth:token-type:access_token";
    public static final String REFRESH_TOKEN_TOKEN_TYPE = "urn:ietf:params:oauth:token-type:refresh_token";
    public static final String OFFLINE_ACCESS_SCOPE = "offline_access";
    public static final String ADDRESS_SCOPE = "address";
    public static final String EMAIL_SCOPE = "email";
    public static final String PROFILE_SCOPE = "email";
    public static final String ROLES_SCOPE = "roles";
}
