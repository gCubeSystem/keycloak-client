package org.gcube.common.keycloak;

import java.net.URL;
import java.util.List;
import java.util.Map;

import org.gcube.common.keycloak.model.JSONWebKeySet;
import org.gcube.common.keycloak.model.PublishedRealmRepresentation;
import org.gcube.common.keycloak.model.TokenIntrospectionResponse;
import org.gcube.common.keycloak.model.TokenResponse;

public interface KeycloakClient {

    public static final String PROD_ROOT_SCOPE = "/d4science.research-infrastructures.eu";
    public static final String OPEN_ID_URI_PATH = "protocol/openid-connect";
    public static final String TOKEN_URI_PATH = "token";
    public static final String JWK_URI_PATH = "certs";
    public static final String TOKEN_INTROSPECT_URI_PATH = "introspect";
    public static final String AVATAR_URI_PATH = "account-avatar";
    public final static String D4S_CONTEXT_HEADER_NAME = "x-d4science-context";
    public final static String D4S_IDENTITY_SCOPE = "d4s-identity";
    public final static String D4S_EU_EXTENDED_PROFILE_SCOPE = "eu_extended_profile";
    public static final String D4S_DYNAMIC_SCOPE_NAME = "d4s-context";
    public static final String D4S_DYNAMIC_SCOPE_NAME_TOKEN_CLAIM = "d4s_context";
    public static final String DEFAULT_DYNAMIC_SCOPE_SEPARATOR = ":";

    public static String DEFAULT_REALM = "d4science";

    /**
     * Replaces the list of the provided OIDC scopes for the next OIDC token requests
     * @param scopes the list of scopes to use in the calls
     * @return the client itself
     */
    KeycloakClient useScopes(List<String> scopes);

    /**
     * Adds the provided OIDC scopes to the list of scopes to use for the next OIDC token requests
     * @param scopes the list of scopes to add
     * @return the client itself
     */
    KeycloakClient addScopes(List<String> scopes);

    /**
     * Adds the dynamic scope to the list of scopes to use for the next OIDC token requests
     * @param dynamicScope the dynamic scope that will be the prefix
     * @param value the value of the dynamic scope
     * @return the client itself
     */
    KeycloakClient addDynamicScope(String dynamicScope, String value);

    /**
     * Removes the provided OIDC scopes from the list of scopes to use for the next OIDC token requests
     * @param scopes the list of scopes to remove
     * @return the client itself
     */
    KeycloakClient removeScopes(List<String> scopes);

    /**
     * Removes all the custom OIDC scopes from the list of scopes to use the next OIDC token requests
     * @return the client itself
     */
    KeycloakClient removeAllScopes();

    /**
     * Sets a flag to use dynamic scope ({@link #D4S_DYNAMIC_SCOPE_NAME} = {@value #D4S_DYNAMIC_SCOPE_NAME}) instead of custom header ({@link #D4S_CONTEXT_HEADER_NAME} = {@value #D4S_CONTEXT_HEADER_NAME})
     * when an OIDC token with context is used
     * @param use use or not use dynamic scope
     * @return the client itself
     */
    KeycloakClient useDynamicScopeInsteadOfCustomHeaderForContextRestricion(boolean useDynamicScopeInsteadOfCustomHeaderForContextRestricion);

    /**
     * Returns the Keycloak base {@link URL} for the given context and the default realm (<code>d4science</code>)
     * 
     * @param context the context where the endpoint is needed (e.g. <code>/gcube</code> for DEV)
     * @return the Keycloak <code>token</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getRealmBaseURL(String context) throws KeycloakClientException;

    /**
     * Returns the Keycloak base {@link URL} for the given context and in the given realm.
     * 
     * @param context the context where the endpoint is needed (e.g. <code>/gcube</code> for DEV)
     * @param realm the realm to use to construct the base URL
     * @return the Keycloak <code>token</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getRealmBaseURL(String context, String realm) throws KeycloakClientException;

    /**
     * Constructs the Keycloak <code>token</code> endpoint {@link URL} from the realm's base URL.
     * 
     * @param realmBaseURL the realm's base URL to use
     * @return the Keycloak <code>token</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getTokenEndpointURL(URL realmBaseURL) throws KeycloakClientException;

    /**
     * Constructs the Keycloak <code>JWK</code> endpoint {@link URL} from the realm's base URL.
     * 
     * @param realmBaseURL the realm's base URL to use
     * @return the Keycloak <code>JWK</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getJWKEndpointURL(URL realmBaseURL) throws KeycloakClientException;

    /**
     * Constructs the Keycloak <code>introspection</code> endpoint {@link URL} from the realm's base URL.
     * 
     * @param realmBaseURL the realm's base URL to use
     * @return the Keycloak <code>introspection</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getIntrospectionEndpointURL(URL realmBaseURL) throws KeycloakClientException;

    /**
     * Compute the keycloak <code>introspection</code> endpoint {@link URL} starting from the provided token endpoint.
     * 
     * @param tokenEndpointURL the token endpoint to use in the compute
     * @return the keycloak <code>introspection</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL computeIntrospectionEndpointURL(URL tokenEndpointURL) throws KeycloakClientException;

    /**
     * Constructs the Keycloak <code>avatar</code> endpoint {@link URL} from the realm's base URL.
     * 
     * @param realmBaseURL the realm's base URL to use
     * @return the Keycloak <code>avatar</code> endpoint URL
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL getAvatarEndpointURL(URL realmBaseURL) throws KeycloakClientException;

    /**
     * Gets the realm info setup (RSA <code>public_key</code>, <code>token-service</code> URL,
     * <code>account-service</code> URL and <code>tokens-not-before</code> setting)
     * 
     * @param realmURL the realm URL
     * @return the configured realm info
     * @throws KeycloakClientException if something goes wrong getting realm info
     */
    PublishedRealmRepresentation getRealmInfo(URL realmURL) throws KeycloakClientException;

    /**
     * Loads the actual JWK from the Keycloak server
     * @param jwkURL the server's jwk URL to use
     * @return an object with JWK details
     * @throws KeycloakClientException  if something goes wrong getting JWK info
     */
    JSONWebKeySet getRealmJSONWebKeySet(URL jwkURL) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the context's Keycloak server, by using provided clientId and client secret.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(String context, String clientId, String clientSecret) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the context's Keycloak server, by using provided clientId and client secret.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(String context, String clientId, String clientSecret, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided clientId and client secret.
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(URL tokenURL, String clientId, String clientSecret) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided clientId and client secret.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(URL tokenURL, String clientId, String clientSecret, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(String context, String authorization) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(String context, String authorization, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(URL tokenURL, String authorization) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(URL tokenURL, String authorization, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token from the context's Keycloak server, by using provided clientId and client secret, reducing the audience to the requested one.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(String context, String clientId, String clientSecret, String audience)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the context's Keycloak server, by using provided clientId and client secret, reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(String context, String clientId, String clientSecret, String audience, Map<String, String> extraHeaders)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided clientId and client secret, reducing the audience to the requested one.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(URL tokenURL, String clientId, String clientSecret, String audience)
            throws KeycloakClientException;
    
    /**
     * Queries an OIDC token from the Keycloak server, by using provided clientId and client secret, reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(URL tokenURL, String clientId, String clientSecret, String audience, Map<String, String> extraHeaders)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization, reducing the audience to the requested one.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(String context, String authorization, String audience)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization, reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(String context, String authorization, String audience, Map<String, String> extraHeaders)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization, reducing the audience to the requested one.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(URL tokenURL, String authorization, String audience)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server, by using provided authorization, reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenWithContext(URL tokenURL, String authorization, String audience, Map<String, String> extraHeaders)
            throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUser(String context, String clientId, String clientSecret, String username,
            String password) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUser(String context, String clientId, String clientSecret, String username,
            String password, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(String context, String authorization, String username,
            String password, String audience) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the Keycloak server, by using provided clientId and client secret and user's username and password, reducing the audience to the requested one.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(String context, String clientId, String clientSecret, String username,
            String password, String audience) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the Keycloak server, by using provided clientId and client secret and user's username and password, reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(String context, String clientId, String clientSecret,
            String username, String password, String audience, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password, reducing the audience to the requested one.
     * 
     * The implementation uses the custom <code>x-d4science-context</code> HTTP header that the proper mapper on Keycloak uses to reduce the audience
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(URL tokenURL, String clientId, String clientSecret, String username,
            String password, String audience) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password, , reducing the audience to the requested one.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(URL tokenURL, String clientId, String clientSecret,
            String username, String password, String audience, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(String context, String authorization, String username,
            String password, String audience, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(URL tokenURL, String authorization, String username, String password,
            String audience) throws KeycloakClientException;

    /**
     * Queries an OIDC token for a specific user from the context's Keycloak server, by using provided clientId and client secret and user's username and password.
     * Optionally extra HTTP headers can be provided to be used in the call.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param username the user's username
     * @param password the user's password
     * @param audience an optional parameter to shrink the token's audience to the requested one (e.g. a specific context), by leveraging on the custom HTTP header and corresponding mapper on Keycloak
     * @param extraHeaders extra HTTP headers to add to the request
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCTokenOfUserWithContext(URL tokenURL, String authorization, String username,
            String password, String audience, Map<String, String> extraHeaders) throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using provided authorization, for the given audience (context),
     * in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience the audience (context) where to request the issuing of the ticket (URLEncoded)
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(String context, String authorization, String audience, List<String> permissions)
            throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using provided authorization, for the given audience (context),
     * in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param authorization the authorization to be set as header (e.g. a "Basic ...." auth or an encoded JWT access token preceded by the "Bearer " string)
     * @param audience the audience (context) where to request the issuing of the ticket (URLEncoded)
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(URL tokenURL, String authorization, String audience, List<String> permissions)
            throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using access-token provided by the {@link TokenResponse} object
     * for the given audience (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param oidcTokenResponse the previously issued token as {@link TokenResponse} object
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(String context, TokenResponse oidcTokenResponse, String audience,
            List<String> permissions) throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using access-token provided by the {@link TokenResponse} object
     * for the given audience (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param oidcTokenResponse the previously issued token as {@link TokenResponse} object
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(URL tokenURL, TokenResponse oidcTokenResponse, String audience,
            List<String> permissions) throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using provided clientId and client secret for the given audience
     * (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(String context, String clientId, String clientSecret, String audience,
            List<String> permissions) throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server, by using provided clientId and client secret for the given audience
     * (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param tokenURL the token endpoint {@link URL} of the Keycloak server
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(URL tokenURL, String clientId, String clientSecret, String audience,
            List<String> permissions) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server using the refresh token JWT encoded string in the
     * token response object.
     * 
     * Client id will be read from "issued for" access token's claim and client secret will be not sent.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String context, TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server using the refresh token JWT encoded string in the
     * token response object.
     * 
     * Client id will be read from "issued for" access token's claim and client secret will be not sent.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(URL tokenURL, TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server using the refresh token JWT encoded string in the
     * token response object and the provided client id and secret.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the requestor client id, may be <code>null</code> and in this case will be take from the access token "issued for" claim
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String context, String clientId, String clientSecret, TokenResponse tokenResponse)
            throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server using the refresh token JWT encoded string in the
     * token response object and the provided client id and secret.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param clientId the requestor client id, may be <code>null</code> and in this case will be take from the access token "issued for" claim
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(URL tokenURL, String clientId, String clientSecret, TokenResponse tokenResponse)
            throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server by using the client id and secret
     * and the refresh token JWT encoded string obtained with the access token in the previous token response.
     *
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param refreshTokenJWTString the previously issued refresh token JWT string
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String context, String clientId, String clientSecret, String refreshTokenJWTString)
            throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server by using the client id and secret
     * and the refresh token JWT encoded string obtained with the access token in the previous token response.
     * 
     * @param tokenURL the token endpoint {@link URL} of the OIDC server
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param refreshTokenJWTString the previously issued refresh token JWT string
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(URL tokenURL, String clientId, String clientSecret, String refreshTokenJWTString)
            throws KeycloakClientException;

    /**
     * Exchanges a token for another access token for a specific client and a specific audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForAccessToken(String context, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws KeycloakClientException;

    /**
     * Exchanges a token for another access token for a specific client and a specific audience
     * 
     * @param tokenURL the token endpoint URL
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForAccessToken(URL tokenURL, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws KeycloakClientException;

    /**
     * Exchanges a token for another access and a refresh tokens for a specific client and a specific audience
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForRefreshToken(String context, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws KeycloakClientException;

    /**
     * Exchanges a token for another access and a refresh tokens for a specific client and a specific audience
     * 
     * @param tokenURL the token endpoint URL
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForRefreshToken(URL tokenURL, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws KeycloakClientException;

    /**
     * Exchanges a token for another access and an offline refresh tokens for a specific client and a specific audience
     * The refresh token will be of the offline type only if the original token has the <code>offline_access</code> within its scopes
     * 
     * @param tokenURL the token endpoint URL
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws IllegalArgumentException if the original token does'nt contains the <code>offline_access</code> scope within its scopes or if is impossible to parse the access token as JSON
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForOfflineToken(String context, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws IllegalArgumentException, KeycloakClientException;

    /**
     * Exchanges a token for another access and an offline refresh tokens for a specific client and a specific audience
     * The refresh token will be of the offline type only if the original token has the scope <code>offline_access</code> within its scopes
     * 
     * @param tokenURL the token endpoint URL
     * @param oidcAccessToken the original access token to exchange
     * @param clientId the authorized client's id
     * @param clientSecret the authorized client's secret
     * @param audience the requested token audience
     * @return the exchanged token response
     * @throws IllegalArgumentException if the original token does'nt contains the <code>offline_access</code> scope within its scopes or if is impossible to parse the access token as JSON
     * @throws KeycloakClientException if an error occurs during the exchange
     */
    TokenResponse exchangeTokenForOfflineToken(URL tokenURL, String oidcAccessToken, String clientId,
            String clientSecret, String audience) throws IllegalArgumentException, KeycloakClientException;

    /**
     * Introspects an access token against the Keycloak server.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return a {@link TokenIntrospectionResponse} object with the introspection results; in particular, the <code>active</code> field represents the token validity
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    TokenIntrospectionResponse introspectAccessToken(String context, String clientId, String clientSecret,
            String accessTokenJWTString) throws KeycloakClientException;

    /**
     * Introspects an access token against the Keycloak server.
     * 
     * @param introspectionURL the introspection endpoint {@link URL} of the Keycloak server
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return a {@link TokenIntrospectionResponse} object with the introspection results; in particular, the <code>active</code> field represents the token validity
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    TokenIntrospectionResponse introspectAccessToken(URL introspectionURL, String clientId, String clientSecret,
            String accessTokenJWTString) throws KeycloakClientException;

    /**
     * Verifies an access token against the Keycloak server.
     * 
     * @param context the context where the Keycloak's is needed (e.g. <code>/gcube</code> for DEV)
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return <code>true</code> if the token is active, <code>false</code> otherwise
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    boolean isAccessTokenVerified(String context, String clientId, String clientSecret, String accessTokenJWTString)
            throws KeycloakClientException;

    /**
     * Verifies an access token against the Keycloak server.
     * 
     * @param introspectionURL the introspection endpoint {@link URL} of the Keycloak server
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return <code>true</code> if the token is active, <code>false</code> otherwise
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    boolean isAccessTokenVerified(URL introspectionURL, String clientId, String clientSecret,
            String accessTokenJWTString) throws KeycloakClientException;

    /**
     * Retrieves the user's avatar image data from Keycloak server.
     * @param context the context used to compute the server endpoint in the correct environment
     * @param tokenResponse the token response where to get the bearer token for the authorization header.
     * @return the avatar's data bytes
     * @throws KeycloakClientException if something goes wrong in the request
     */
    byte[] getAvatarData(String context, TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Retrieves the user's avatar image data from Keycloak server.
     * @param avatarURL the server's avatar endpoint URL
     * @param tokenResponse the token response where to get the bearer token for the authorization header.
     * @return the avatar's data bytes
     * @throws KeycloakClientException if something goes wrong in the request
     */
    byte[] getAvatarData(URL avatarURL, TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Retrieves the user's avatar image data from Keycloak server.
     * @param avatarURL the server's avatar endpoint URL
     * @param the string to user as authorization header (e.g. 'bearer xxxx')
     * @return the avatar's data bytes
     * @throws KeycloakClientException if something goes wrong in the request
     */
    byte[] getAvatarData(URL avatarURL, String authorization) throws KeycloakClientException;

}