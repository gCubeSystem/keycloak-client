package org.gcube.common.keycloak;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeycloakClientFactory {

    protected static final Logger logger = LoggerFactory.getLogger(KeycloakClientFactory.class);

    protected static String CUSTOM_BASE_URL;

    public static void setCustomBaseURL(String customBaseURL) {
        if (customBaseURL != null) {
            logger.info("Setting custom base URL static value to {}", customBaseURL);
        } else {
            logger.info("Removing custom base URL static value");
        }
        if (customBaseURL == null || customBaseURL.endsWith("/")) {
            CUSTOM_BASE_URL = customBaseURL;
        } else {
            CUSTOM_BASE_URL = customBaseURL += "/";
        }
    }

    public static String getCustomBaseURL() {
        return CUSTOM_BASE_URL;
    }

    public static KeycloakClient newInstance() {
        logger.debug("Instantiating a new keycloak client instance");
        DefaultKeycloakClient newInstance = new DefaultKeycloakClient();
        if (getCustomBaseURL() != null) {
            newInstance.setCustomBaseURL(CUSTOM_BASE_URL);
        }
        return newInstance;
    }
 
}