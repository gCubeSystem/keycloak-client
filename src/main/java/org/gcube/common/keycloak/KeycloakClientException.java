package org.gcube.common.keycloak;

public class KeycloakClientException extends Exception {

    private static final long serialVersionUID = -1615745541003534684L;

    private int status = -1;
    private String contentType = null;
    private String responseString = null;

    public static KeycloakClientException create(String message, int status, String contentType,
            String textResponse) {

        return create(message, status, contentType, textResponse, null);
    }

    public static KeycloakClientException create(String message, int status, String contentType,
            String textResponse, Exception cause) {

        String exMessage = "[" + status + "] " + message + " (" + contentType + "): " + textResponse;
        KeycloakClientException e = cause != null ? new KeycloakClientException(exMessage, cause)
                : new KeycloakClientException(exMessage);

        e.setStatus(status);
        e.setContentType(contentType);
        e.setResponseString(textResponse);
        return e;
    }

    public KeycloakClientException() {
        super();
    }

    public KeycloakClientException(String message) {
        super(message);
    }

    public KeycloakClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public boolean hasJSONPayload() {
        return getContentType() != null && getContentType().endsWith("json");
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    public String getResponseString() {
        return responseString;
    }

}
