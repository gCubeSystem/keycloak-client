package org.gcube.common.keycloak;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Collections;

import org.gcube.common.keycloak.model.JSONWebKeySet;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.PublishedRealmRepresentation;
import org.gcube.common.keycloak.model.TokenIntrospectionResponse;
import org.gcube.common.keycloak.model.TokenResponse;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestKeycloakClient {

    protected static final Logger logger = LoggerFactory.getLogger(TestKeycloakClient.class);

    protected static final String ROOT_CONTEXT = "/gcube";
//    protected static final String ROOT_CONTEXT = "/pred4s";
    protected static final String BASE_URL = "https://url.gcube.d4science.org/auth/realms/d4science";
//    protected static final String BASE_URL = "https://url.pred4s.d4science.org/auth/realms/d4science";
    protected static final String TOKEN_ENDPOINT = BASE_URL + "/protocol/openid-connect/token";
    protected static final String INTROSPECTION_ENDPOINT = TOKEN_ENDPOINT + "/introspect";
    protected static final String AVATAR_ENDPOINT = BASE_URL + "/account-avatar";

    protected static final String ACCOUNTS_INTROSPECTION_ENDPOINT = "https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token/introspect";
//    protected static final String ACCOUNTS_INTROSPECTION_ENDPOINT = "https://accounts.pre.d4science.org/auth/realms/d4science/protocol/openid-connect/token/introspect";

    protected static final String GATEWAY = "next.dev.d4science.org";
//    protected static final String GATEWAY = "next.pre.d4science.org";
    protected static final String TOKEN_RESTRICTION_VRE_CONTEXT = "%2Fgcube%2Fdevsec%2FCCP";
    protected static final String CLIENT_ID = "keycloak-client-unit-test";
    protected static final String CLIENT_SECRET = "ebf6f82e-9511-408e-8321-203081e472d8";
//    protected static final String CLIENT_SECRET = "afBhqRYUj2C0bNggdNCQOFRYbcb8hR1p"; // For PRE
    protected static final String TEST_AUDIENCE = "conductor-server";
    protected static final String TEST_USER_USERNAME = "testuser";
    protected static final String TEST_USER_PASSWORD = "t35tp455";
    protected static final String OLD_OIDC_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NTI5Nzk4NDUsImlhdCI6MTY1Mjk3OTU0NSwianRpIjoiMzQ2MjgwMWItODg4NS00YTM4LWJkNDUtNWExM2U1MGE5MGU5IiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5kZXYuZDRzY2llbmNlLm9yZy9hdXRoL3JlYWxtcy9kNHNjaWVuY2UiLCJhdWQiOlsiJTJGZ2N1YmUiLCIlMkZnY3ViZSUyRmRldnNlYyUyRmRldlZSRSIsImFjY291bnQiXSwic3ViIjoiYTQ3ZGZlMTYtYjRlZC00NGVkLWExZDktOTdlY2Q1MDQzNjBjIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoia2V5Y2xvYWstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6ImQ4MDk3MDBmLWEyNDUtNDI3Zi1hYzhjLTQxYjFkZDNkYTQ3MCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsIkluZnJhc3RydWN0dXJlLUNsaWVudCIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiJTJGZ2N1YmUiOnsicm9sZXMiOlsiTWVtYmVyIl19LCJrZXljbG9hay1jbGllbnQiOnsicm9sZXMiOlsidW1hX3Byb3RlY3Rpb24iXX0sIiUyRmdjdWJlJTJGZGV2c2VjJTJGZGV2VlJFIjp7InJvbGVzIjpbIk1lbWJlciJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiY2xpZW50SWQiOiJrZXljbG9hay1jbGllbnQiLCJjbGllbnRIb3N0IjoiOTMuNjYuMTg1Ljc1IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZXJ2aWNlLWFjY291bnQta2V5Y2xvYWstY2xpZW50IiwiY2xpZW50QWRkcmVzcyI6IjkzLjY2LjE4NS43NSJ9.FQu4ox2HWeqeaY7nHYVGeJVpkJOcASfOb8tbOUeG-GB6sMjRB2S8PjLLaw63r_c42yxKszP04XdxGqIWqXTtoD9QCiUHTT5yJTkIpio4tMMGHth9Fbx-9dwk0yy_IFi1_OsCvZFmOQRdjMuUkj1lSqslCzAw-2E5q1Zt415-au5pEVJYNTFqIsG72ChJwh6eq1Dh1XBy8krb7YVPQyIwxO_awgAYO5hbsdvXYlRfCrnB38kk2V6-CQ-XYoL1m7xIB-gjhKCiFvDmmntQSRCZFgb0qi8eOmh9FdzPxZgx7yPJwAAj17dS4B_gz9FpZBVciNzpA6Lf4P2bqvoD9-R6ow";
    protected static final String OLD_UMA_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NTI5ODA0NzgsImlhdCI6MTY1Mjk4MDE3OCwianRpIjoiNjBkNzU3MGMtZmQxOC00NGQ1LTg1MzUtODhlMmFmOGQ1ZTgwIiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5kZXYuZDRzY2llbmNlLm9yZy9hdXRoL3JlYWxtcy9kNHNjaWVuY2UiLCJhdWQiOiJjb25kdWN0b3Itc2VydmVyIiwic3ViIjoiYTQ3ZGZlMTYtYjRlZC00NGVkLWExZDktOTdlY2Q1MDQzNjBjIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoia2V5Y2xvYWstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6IjI3NDUyN2M5LWNkZjMtNGM2Yi1iNTUxLTFmMTRkZGE5ZGVlZiIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiSW5mcmFzdHJ1Y3R1cmUtQ2xpZW50Il19LCJhdXRob3JpemF0aW9uIjp7InBlcm1pc3Npb25zIjpbeyJzY29wZXMiOlsiZ2V0Il0sInJzaWQiOiIyNDlmZDQ2OS03OWM1LTRiODUtYjE5NS1mMjliM2ViNjAzNDUiLCJyc25hbWUiOiJtZXRhZGF0YSJ9LHsic2NvcGVzIjpbImdldCIsInN0YXJ0IiwidGVybWluYXRlIl0sInJzaWQiOiJhNmYzZWFkZS03NDA0LTRlNWQtOTA3MC04MDBhZGI1YWFjNGUiLCJyc25hbWUiOiJ3b3JrZmxvdyJ9LHsicnNpZCI6IjFiNmMwMGI3LTkxMzktNGVhYS1hYWM3LTIwMjMxZmVlMDVhNSIsInJzbmFtZSI6IkRlZmF1bHQgUmVzb3VyY2UifV19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJjbGllbnRJZCI6ImtleWNsb2FrLWNsaWVudCIsImNsaWVudEhvc3QiOiI5My42Ni4xODUuNzUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC1rZXljbG9hay1jbGllbnQiLCJjbGllbnRBZGRyZXNzIjoiOTMuNjYuMTg1Ljc1In0.Hh62E56R-amHwoDPFQEylMvrvmNzWnC_4bDI7_iQYAPJ5YzCNH9d7zcdGaQ96kRmps_JRc2Giv_1W9kYorOhlXl-5QLDrSoqrqFxrNpEGG5r5jpNJbusbu4wNUKiCt_GMnM1UmztgXiQeuggNGkmeBIjotj0eubnmIbUV9ukHj3v7Z5PwNKKX3BCpsghd1u8lg6Nfqk_Oho4GXUfdaFY_AR3SNqzVI_9YLhND_a03MNNWlnfOvj8T4nDCKBZIs91tVyiu98d2TjnQt8PdlVwokMP3LA58m0Khy2cmUm1KF2k0zlzP8MxV9wTxNrpovMr-PnbtEPZ_IlVQIzHwjHfwQ";

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        // Assure to reset the factory's default base URL
        KeycloakClientFactory.setCustomBaseURL(null);
    }

    @Test
    public void test00TokenEndpointConstruction() throws Exception {
        logger.info(
                "*** [0.0] Start testing Keycloak token endpoint construction from base URL computed from context...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        logger.info("*** [0.0] Constructed token URL is: {}", tokenURL);
        URL devTokenURL = new URL(TOKEN_ENDPOINT);
        logger.info("*** [0.0] DEV token URL is: {}", devTokenURL);
        Assert.assertNotNull(tokenURL);
        Assert.assertEquals(tokenURL.getProtocol(), "https");
        Assert.assertEquals(devTokenURL.toString(), tokenURL.toString());
    }

    @Test
    public void test01IntrospectionEndpointConstruction() throws Exception {
        logger.info("*** [0.1] Start testing Keycloak introspection endpoint construction from base URL...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        logger.info("*** [0.1] Constructed introspection URL is: {}", introspectionURL);
        URL devIntrospectionURL = new URL(INTROSPECTION_ENDPOINT);
        logger.info("*** [0.1] DEV introspection URL is: {}", devIntrospectionURL);
        Assert.assertNotNull(introspectionURL);
        Assert.assertEquals(introspectionURL.getProtocol(), "https");
        Assert.assertEquals(devIntrospectionURL.toString(), introspectionURL.toString());
        logger.info("Constructed URL is: {}", introspectionURL);
    }

    @Test
    public void test02IntrospectEndpointCompute() throws Exception {
        logger.info("*** [0.2] Start testing Keycloak userinfo endpoint computed from provided URL string...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        logger.info("*** [0.2] Constructed introspection URL is: {}", introspectionURL);
        URL computedIntrospectionURL = client
                .computeIntrospectionEndpointURL(client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT)));
        logger.info("*** [0.2] Computed introspection URL is: {}", computedIntrospectionURL);
        URL devIntrospectionURL = new URL(INTROSPECTION_ENDPOINT);
        logger.info("*** [0.2] DEV introspection URL is: {}", devIntrospectionURL);
        Assert.assertNotNull(computedIntrospectionURL);
        Assert.assertEquals(computedIntrospectionURL.getProtocol(), "https");
        Assert.assertEquals(introspectionURL.toString(), computedIntrospectionURL.toString());
        Assert.assertEquals(devIntrospectionURL.toString(), computedIntrospectionURL.toString());
    }

    @Test
    public void test03AvatarEndpointConstruction() throws Exception {
        logger.info("*** [0.3] Start testing Keycloak avatar endpoint construction from base URL...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL avatarURL = client.getAvatarEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        logger.info("*** [0.3] Constructed avatar URL is: {}", avatarURL);
        URL devAvatarURL = new URL(AVATAR_ENDPOINT);
        logger.info("*** [0.3] DEV avatar URL is: {}", devAvatarURL);
        Assert.assertNotNull(avatarURL);
        Assert.assertEquals(avatarURL.getProtocol(), "https");
        Assert.assertEquals(devAvatarURL.toString(), avatarURL.toString());
    }

    @Test
    public void test04CustomEndpointTest() throws Exception {
        String customBase = "https://accounts.cloud.dev.d4science.org/auth/realms/";
        logger.info("*** [0.4] Start testing Keycloak token endpoint construction from custom base URL {}...",
                customBase);

        KeycloakClientFactory.setCustomBaseURL(customBase);
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL customBaseURL = client.getRealmBaseURL(ROOT_CONTEXT);
        logger.info("*** [0.4] Constructed token URL is: {}", customBaseURL);
        URL devTokenURL = new URL(TOKEN_ENDPOINT);
        logger.info("*** [0.4] DEV token URL is: {}", devTokenURL);
        Assert.assertNotNull(customBaseURL);
        Assert.assertEquals(customBaseURL.getProtocol(), "https");
        Assert.assertEquals(customBase + KeycloakClient.DEFAULT_REALM + "/", customBaseURL.toString());
    }

    @Test
    public void test10aQueryRealmInfo() throws Exception {
        logger.info("*** [1.0a] Start testing query realm info...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        PublishedRealmRepresentation realmInfo = client.getRealmInfo(client.getRealmBaseURL(ROOT_CONTEXT));

        logger.info("*** [1.0a] Realm info public key PEM: {}", realmInfo.getPublicKeyPem());
        logger.info("*** [1.0a] Realm info public key: {}", realmInfo.getPublicKey());
    }

    @Test
    public void test10bQueryRealmJWK() throws Exception {
        logger.info("*** [1.0b] Start testing query realm JWK...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        JSONWebKeySet jsonWebKeySet = client
                .getRealmJSONWebKeySet(client.getJWKEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT)));

        for (int i = 0; i < jsonWebKeySet.getKeys().length; i++) {
            logger.info("*** [1.0b] Realm JWK public key {} algorithm: {}", i,
                    jsonWebKeySet.getKeys()[i].getAlgorithm());
        }
    }

    @Test
    public void test11TestAccessTokenJWTSignature() throws Exception {
        logger.info("*** [1.0] Start testing access token JTW signature with model utils...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        PublishedRealmRepresentation realmInfo = client.getRealmInfo(client.getRealmBaseURL(ROOT_CONTEXT));

        logger.info("*** [1.0] Realm info public key PEM: {}", realmInfo.getPublicKeyPem());
        logger.info("*** [1.0] Realm info public key: {}", realmInfo.getPublicKey());

        TokenResponse oidcTR = client.queryOIDCToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET);
        logger.info("*** [1.0] OIDC access token: {}", oidcTR.getAccessToken());

        Assert.assertTrue("Access token is not valid",
                ModelUtils.isValid(oidcTR.getAccessToken(), realmInfo.getPublicKey()));
    }

    @Test
    public void test12QueryOIDCToken() throws Exception {
        logger.info("*** [1.2] Start testing query OIDC token from Keycloak with context...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance().queryOIDCToken(ROOT_CONTEXT, CLIENT_ID,
                CLIENT_SECRET);

        logger.info("*** [1.2] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.2] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), "service-account-" + CLIENT_ID, false);
    }

    @Test
    public void test12aQueryOIDCToken() throws Exception {
        logger.info("*** [1.2a] Start testing query OIDC token from Keycloak with URL...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);

        logger.info("*** [1.2a] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.2a] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), "service-account-" + CLIENT_ID, false);
    }

    @Test
    public void test13QueryOIDCTokenOfUser() throws Exception {
        logger.info("*** [1.3] Start testing query OIDC token from Keycloak with context for user...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance().queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID,
                CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [1.3] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.3] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, false);
    }

    @Test
    public void test13aQueryOIDCTokenOfUserWithContext() throws Exception {
        logger.info("*** [1.3a] Start testing query OIDC token for audience from Keycloak with context for user...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance().queryOIDCTokenOfUser(ROOT_CONTEXT,
                CLIENT_ID, CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [1.3a] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.3a] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);

        TokenResponse oidcRestrictedTR = KeycloakClientFactory.newInstance().queryOIDCTokenOfUserWithContext(
                ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD,
                TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("*** [1.3a] OIDC restricted access token: {}", oidcRestrictedTR.getAccessToken());
        logger.info("*** [1.3a] OIDC restricted refresh token: {}", oidcRestrictedTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkTokenResponse(oidcRestrictedTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcRestrictedTR), TEST_USER_USERNAME, true);
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length > 1);
        assertTrue(ModelUtils.getAccessTokenFrom(oidcRestrictedTR).getAudience().length == 1);
        assertTrue(
                TOKEN_RESTRICTION_VRE_CONTEXT.equals(ModelUtils.getAccessTokenFrom(oidcRestrictedTR).getAudience()[0]));
    }

    @Test
    public void test13bQueryOIDCTokenAndUMAOfUser() throws Exception {
        logger.info("*** [1.3b] Start testing query OIDC and UMA tokens from Keycloak with context for user...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [1.3b] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.3b] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, false);

        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, oidcTR, TEST_AUDIENCE, null);

        logger.info("*** [1.3b] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [1.3b] UMA refresh token: {}", umaTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(umaTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR), TEST_USER_USERNAME, true);
    }

    @Test
    public void test13cQueryOIDCTokenOfUserWithContextAndCustomHeader() throws Exception {
        logger.info(
                "*** [1.3c] Start testing query OIDC token for audience from Keycloak with context and custom headers for user...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance().queryOIDCTokenOfUserWithContext(ROOT_CONTEXT,
                CLIENT_ID, CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT,
                Collections.singletonMap("X_A_CUSTOM_HEADER", "HEADER_VALUE"));

        logger.info("*** [1.3c] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.3c] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test14aQueryOIDCTokenWithDynamicScope() throws Exception {
        logger.info(
                "*** [1.4a] Start testing query OIDC token with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .addDynamicScope(KeycloakClient.D4S_DYNAMIC_SCOPE_NAME, TOKEN_RESTRICTION_VRE_CONTEXT)
                .queryOIDCToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET);

        logger.info("*** [1.4a] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.4a] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), "service-account-" + CLIENT_ID, true);
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test14bQueryOIDCTokenWithDynamicScopeWithFlagMode() throws Exception {
        logger.info(
                "*** [1.4b] Start testing query OIDC token with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(true)
                .queryOIDCTokenWithContext(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("*** [1.4b] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.4b] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), "service-account-" + CLIENT_ID, true);
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test15aQueryOIDCTokenForUserWithDynamicScope() throws Exception {
        logger.info(
                "*** [1.5a] Start testing query OIDC token for user on private client with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .addDynamicScope(KeycloakClient.D4S_DYNAMIC_SCOPE_NAME, TOKEN_RESTRICTION_VRE_CONTEXT)
                .queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_USER_USERNAME,
                        TEST_USER_PASSWORD);

        logger.info("*** [1.5a] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.4a] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        assertEquals(CLIENT_ID, ModelUtils.getAccessTokenFrom(oidcTR).getIssuedFor());
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test15bQueryOIDCTokenForUserWithDynamicScopeWithFlagMode() throws Exception {
        logger.info(
                "*** [1.5b] Start testing query OIDC token for user on private client with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(true)
                .queryOIDCTokenOfUserWithContext(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_USER_USERNAME,
                        TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("*** [1.5b] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.5b] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        assertEquals(CLIENT_ID, ModelUtils.getAccessTokenFrom(oidcTR).getIssuedFor());
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test16aQueryOIDCTokenForUserOnPublicClientWithDynamicScope() throws Exception {
        logger.info(
                "*** [1.6a] Start testing query OIDC token for user on a public client with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .addDynamicScope(KeycloakClient.D4S_DYNAMIC_SCOPE_NAME, TOKEN_RESTRICTION_VRE_CONTEXT)
                .queryOIDCTokenOfUser(ROOT_CONTEXT, GATEWAY, null, TEST_USER_USERNAME,
                        TEST_USER_PASSWORD);

        logger.info("*** [1.6a] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.6a] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        assertEquals(GATEWAY, ModelUtils.getAccessTokenFrom(oidcTR).getIssuedFor());
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test16bQueryOIDCTokenForUserOnPublicClientWithDynamicScopeWithFlagMode() throws Exception {
        logger.info(
                "*** [1.6b] Start testing query OIDC token for user on a public client with dynamic scope...");
        TokenResponse oidcTR = KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(true)
                .queryOIDCTokenOfUserWithContext(ROOT_CONTEXT, GATEWAY, null, TEST_USER_USERNAME,
                        TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("*** [1.6b] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.6b] OIDC refresh token: {}", oidcTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(oidcTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR), TEST_USER_USERNAME, true);
        assertEquals(GATEWAY, ModelUtils.getAccessTokenFrom(oidcTR).getIssuedFor());
        assertTrue(ModelUtils.getAccessTokenFrom(oidcTR).getAudience().length == 1);
        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, ModelUtils.getAccessTokenFrom(oidcTR).getAudience()[0]);
        // It is not possible to check programmatically if the header has been added to the call, See the logs if the Header is present.
    }

    @Test
    public void test24QueryUMAToken() throws Exception {
        logger.info("*** [2.4] Start testing query UMA token from Keycloak with context...");
        TokenResponse umaTR = KeycloakClientFactory.newInstance().queryUMAToken(ROOT_CONTEXT, CLIENT_ID,
                CLIENT_SECRET, TEST_AUDIENCE, null);

        logger.info("*** [2.4] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [2.4] UMA refresh token: {}", umaTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(umaTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR), "service-account-" + CLIENT_ID, true);
    }

    @Test
    public void test24aQueryUMAToken() throws Exception {
        logger.info("*** [2.4a] Start testing query UMA token from Keycloak with URL...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse umaTR = client.queryUMAToken(tokenURL, CLIENT_ID, CLIENT_SECRET, TEST_AUDIENCE, null);

        logger.info("*** [2.4a] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [2.4a] UMA refresh token: {}", umaTR.getRefreshToken());

        TestModelUtils.checkTokenResponse(umaTR);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR), "service-account-" + CLIENT_ID, true);
    }

    @Test
    public void test302IntrospectOIDCAccessToken() throws Exception {
        logger.info("*** [3.2] Start testing introspect OIDC access token...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);
        TokenIntrospectionResponse tir = client.introspectAccessToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                oidcTR.getAccessToken());

        TestModelUtils.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test302aIntrospectOIDCAccessToken() throws Exception {
        logger.info("*** [3.2a] Start testing introspect OIDC access token...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenIntrospectionResponse tir = client.introspectAccessToken(introspectionURL, CLIENT_ID, CLIENT_SECRET,
                oidcTR.getAccessToken());

        TestModelUtils.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test302bIntrospectOIDCAccessTokenFromDifferentServer() throws Exception {
        logger.info("*** [3.2b] Start testing introspect OIDC access token with different base URLs...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);

        logger.info("*** [3.2b] OIDC access token: {}", oidcTR.getAccessToken());

        URL introspectionURL = new URL(ACCOUNTS_INTROSPECTION_ENDPOINT);
        TokenIntrospectionResponse tir = client.introspectAccessToken(introspectionURL, CLIENT_ID, CLIENT_SECRET,
                oidcTR.getAccessToken());

        TestModelUtils.checkTokenIntrospectionResponse(tir, true);
    }

    @Test
    public void test304IntrospectUMAAccessToken() throws Exception {
        logger.info("*** [3.4] Start testing introspect UMA access token...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_AUDIENCE, null);
        TokenIntrospectionResponse tir = client.introspectAccessToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                umaTR.getAccessToken());

        TestModelUtils.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test304aIntrospectUMAAccessToken() throws Exception {
        logger.info("*** [3.4a] Start testing introspect UMA access token...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_AUDIENCE, null);
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenIntrospectionResponse tir = client.introspectAccessToken(introspectionURL, CLIENT_ID, CLIENT_SECRET,
                umaTR.getAccessToken());

        TestModelUtils.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test306OIDCAccessTokenVerification() throws Exception {
        logger.info("*** [3.6] Start OIDC access token verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);
        Assert.assertTrue(
                client.isAccessTokenVerified(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, oidcTR.getAccessToken()));
    }

    @Test
    public void test306aOIDCAccessTokenVerification() throws Exception {
        logger.info("*** [3.6a] Start OIDC access token verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL tokenURL = client.getTokenEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        TokenResponse oidcTR = client.queryOIDCToken(tokenURL, CLIENT_ID, CLIENT_SECRET);
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        Assert.assertTrue(
                client.isAccessTokenVerified(introspectionURL, CLIENT_ID, CLIENT_SECRET, oidcTR.getAccessToken()));
    }

    @Test
    public void test307OIDCAccessTokenNonVerification() throws Exception {
        logger.info("*** [3.7] Start OIDC access token NON verification...");
        Assert.assertFalse(KeycloakClientFactory.newInstance().isAccessTokenVerified(
                ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, OLD_OIDC_ACCESS_TOKEN));
    }

    @Test
    public void test307aOIDCAccessTokenNonVerification() throws Exception {
        logger.info("*** [3.7a] Start OIDC access token NON verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        Assert.assertFalse(
                client.isAccessTokenVerified(introspectionURL, CLIENT_ID, CLIENT_SECRET, OLD_OIDC_ACCESS_TOKEN));
    }

    @Test
    public void test309UMAAccessTokenVerification() throws Exception {
        logger.info("*** [3.9] Start UMA access token verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_AUDIENCE, null);
        Assert.assertTrue(
                client.isAccessTokenVerified(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, umaTR.getAccessToken()));
    }

    @Test
    public void test309aUMAAccessTokenVerification() throws Exception {
        logger.info("*** [3.9a] Start UMA access token verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, TEST_AUDIENCE, null);
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        Assert.assertTrue(
                client.isAccessTokenVerified(introspectionURL, CLIENT_ID, CLIENT_SECRET, umaTR.getAccessToken()));
    }

    @Test
    public void test310UMAAccessTokenNonVerification() throws Exception {
        logger.info("*** [3.10] Start UMA access token NON verification...");
        Assert.assertFalse(KeycloakClientFactory.newInstance().isAccessTokenVerified(
                ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, OLD_UMA_ACCESS_TOKEN));
    }

    @Test
    public void test310aUMAAccessTokenNonVerification() throws Exception {
        logger.info("*** [3.10a] Start UMA access token NON verification...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        URL introspectionURL = client.getIntrospectionEndpointURL(client.getRealmBaseURL(ROOT_CONTEXT));
        Assert.assertFalse(
                client.isAccessTokenVerified(introspectionURL, CLIENT_ID, CLIENT_SECRET, OLD_UMA_ACCESS_TOKEN));
    }

    @Test
    public void test4GetTokenOfUserWithHelper() throws Exception {
        logger.info("*** [4] Start testing query token from Keycloak for user...");
        TokenResponse tokenResponse = KeycloakClientHelper.getTokenForUser(ROOT_CONTEXT, TEST_USER_USERNAME,
                TEST_USER_PASSWORD);

        logger.info("*** [4] UMA access token: {}", tokenResponse.getAccessToken());
        logger.info("*** [4] UMA refresh token: {}", tokenResponse.getRefreshToken());

        TestModelUtils.checkTokenResponse(tokenResponse);
        TestModelUtils.checkAccessToken(ModelUtils.getAccessTokenFrom(tokenResponse), TEST_USER_USERNAME, true);
    }

    @Test
    public void test51ExchangeToken4Access() throws Exception {
        logger.info("*** [5.1] Start testing token exchange for access token from Keycloak...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [5.1] OIDC access token: {}", oidcTR.getAccessToken());

        TokenResponse exchangedTR = client.exchangeTokenForAccessToken(ROOT_CONTEXT, oidcTR.getAccessToken(),
                CLIENT_ID, CLIENT_SECRET, CLIENT_ID);

        logger.info("*** [5.1] Exchanged access token: {}", exchangedTR.getAccessToken());
        TestModelUtils.checkTokenResponse(exchangedTR, false);
        Assert.assertNull(exchangedTR.getRefreshToken());

        TestModelUtils.checkTokenIntrospectionResponse(client.introspectAccessToken(ROOT_CONTEXT, CLIENT_ID,
                CLIENT_SECRET, exchangedTR.getAccessToken()));
    }

    @Test
    public void test51aExchangeUMAToken4Access() throws Exception {
        logger.info("*** [5.1a] Start testing UMA token exchange for access token from Keycloak...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [5.1a] OIDC access token: {}", oidcTR.getAccessToken());

        TokenResponse umaTR = client.queryUMAToken(ROOT_CONTEXT, oidcTR, TOKEN_RESTRICTION_VRE_CONTEXT, null);

        logger.info("*** [5.1a] UMA access token: {}", umaTR.getAccessToken());

        TokenResponse exchangedTR = client.exchangeTokenForAccessToken(ROOT_CONTEXT, umaTR.getAccessToken(),
                CLIENT_ID, CLIENT_SECRET, CLIENT_ID);

        logger.info("*** [5.1a] Exchanged access token: {}", exchangedTR.getAccessToken());
        TestModelUtils.checkTokenResponse(exchangedTR, false);
        Assert.assertNull(exchangedTR.getRefreshToken());

        TestModelUtils.checkTokenIntrospectionResponse(client.introspectAccessToken(ROOT_CONTEXT, CLIENT_ID,
                CLIENT_SECRET, exchangedTR.getAccessToken()));
    }

    @Test
    public void test52ExchangeToken4refresh() throws Exception {
        logger.info("*** [5.2] Start testing token exchange for refresh token from Keycloak...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [5.2] OIDC access token: {}", oidcTR.getAccessToken());

        TokenResponse exchangedTR = client.exchangeTokenForRefreshToken(ROOT_CONTEXT, oidcTR.getAccessToken(),
                CLIENT_ID, CLIENT_SECRET, CLIENT_ID);

        logger.info("*** [5.2] Exchanged access token: {}", exchangedTR.getAccessToken());
        logger.info("*** [5.2] Exchanged refresh token: {}", exchangedTR.getRefreshToken());
        TestModelUtils.checkTokenResponse(exchangedTR);

        TestModelUtils.checkTokenIntrospectionResponse(
                client.introspectAccessToken(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET, exchangedTR.getAccessToken()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test53ExchangeToken4Offline() throws Exception {
        logger.info("*** [5.3] Start testing token exchange for offline token from Keycloak...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("*** [5.3] OIDC access token: {}", oidcTR.getAccessToken());

        //        TokenResponse exchangedTR = 
        client.exchangeTokenForOfflineToken(ROOT_CONTEXT, oidcTR.getAccessToken(),
                CLIENT_ID, CLIENT_SECRET, CLIENT_ID);

        // For the moment this part is not covered by tests
        //        logger.info("*** [5.3] Exchanged access token: {}", exchangedTR.getAccessToken());
        //        logger.info("*** [5.3] Exchanged refresh token: {}", exchangedTR.getRefreshToken());
        //        TestModelUtils.checkTokenResponse(exchangedTR, true);
        //        TestModelUtils.checkOfflineToken(exchangedTR);
        //
        //        TestModelUtils.checkTokenIntrospectionResponse(client.introspectAccessToken(DEV_ROOT_CONTEXT, CLIENT_ID,
        //                CLIENT_SECRET, exchangedTR.getAccessToken()));

    }

    @Test
    public void test6GetAvatar() throws Exception {
        logger.info("*** [6] Start testing get user's avatar...");
        KeycloakClient client = KeycloakClientFactory.newInstance();
        TokenResponse oidcTR = client.queryOIDCTokenOfUser(ROOT_CONTEXT, CLIENT_ID, CLIENT_SECRET,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        byte[] avatarData = client.getAvatarData(ROOT_CONTEXT, oidcTR);
        Assert.assertNotNull("Avatar data is null", avatarData);
        logger.info("*** [6] Avatar image of user is {} bytes", avatarData.length);
    }
}
